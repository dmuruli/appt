<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Appointment Scheduling </title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<!--  Full Calendar libraries -->
<link href='fullcalendar/lib/fullcalendar.min.css' rel='stylesheet' />
<link href='fullcalendar/lib/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='fullcalendar/scheduler.min.css' rel='stylesheet' />

<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery.min.js'></script>
  <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/lib/moment.min.js'></script>
<script src="http://fullcalendar.io/js/fullcalendar-2.1.1/lib/jquery-ui.custom.min.js"></script>
<script src='http://fullcalendar.io/js/fullcalendar-2.1.1/fullcalendar.min.js'></script>


<link href='fullcalendar/lib/fullcalendar.min.css' rel='stylesheet' />
<link href='fullcalendar/lib/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='fullcalendar/scheduler.min.css' rel='stylesheet' />
<script src='fullcalendar/lib/moment.min.js'></script>
<script src='fullcalendar/lib/jquery.min.js'></script>
<script src='fullcalendar/lib/fullcalendar.min.js'></script>
<script src='fullcalendar/scheduler.min.js'></script>
<script>

$(document).ready(function() { // document ready

		$('#calendar').fullCalendar({
			schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
			now: new Date(),
			editable: true,
			aspectRatio: 1.8,
			scrollTime: '00:00',
			header: {
				left: 'promptResource today prev,next',
				center: 'title',
				right: 'timelineDay,timelineThreeDays,agendaWeek,month'
			},
			customButtons: {
				promptResource: {
					text: '+ Service Provider',
					click: function() {
						var title = prompt('Sales Person');
						if (title) {
							$('#calendar').fullCalendar(
								'addResource',
								{ title: title },
								true // scroll to the new resource?
							);
						}
					}
				}
			},
			defaultView: 'timelineDay',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 3 }
				}
			},
			resourceLabelText: 'Service Provider',
			resourceRender: function(resource, cellEls) {
				cellEls.on('click', function() {
					if (confirm('Are you sure you want to delete ' + resource.title + '?')) {
						$('#calendar').fullCalendar('removeResource', resource);
					}
				});
			},

			resources:'/apptweb/rest/resources' ,
			 type: 'GET',
			events:{
		        url: '/apptweb/rest/events',
		        data: function() { // a function that returns an object
		            return {
		                specialityId: '345'
		            };
		        }
		    }
		});

	});

</script>
<style>

	body {
		margin: 0;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	p {
		text-align: center;
	}

	#calendar {
		max-width: 900px;
		margin: 50px auto;
	}

	.fc-resource-area td {
		cursor: pointer;
	}

</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Scheduling Admin Page</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

                    </ul>
                </li>


            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                    <li>
                        <a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Clinic Location Admin</a>
                    </li>
                    <li>
                        <a href="tables.html"><i class="fa fa-fw fa-table"></i> Application User Admin</a>
                    </li>
                    <li>
                        <a href="forms.html"><i class="fa fa-fw fa-edit"></i>Appointment Notfication Reports</a>
                    </li>


                    <li class="active">
                        <a href="blank-page.html"><i class="fa fa-fw fa-file"></i> Mountain View Clinic</a>
                    </li>
                    <li>
                        <a href="index-rtl.html"><i class="fa fa-fw fa-dashboard"></i> Santa Clara Clinic</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Mountain View Clinic
                            <small>Physiotherapist Scheduling</small>
                        </h1>

                    </div>
                </div>
                <!-- /.row -->
				<div id='calendar'></div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->





</body>

</html>
