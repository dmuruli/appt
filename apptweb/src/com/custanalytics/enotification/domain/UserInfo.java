package com.custanalytics.enotification.domain;

import java.io.Serializable;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import com.custanalytics.enotification.entity.Account;
import com.custanalytics.enotification.entity.Provider;

@Component
public class UserInfo implements Serializable{
	DateTime loginTime;
	Provider provider;
	Account account;
	String dashboardStatusMessage;

	public String getDashboardStatusMessage() {
		return dashboardStatusMessage;
	}

	public void setDashboardStatusMessage(String dashboardStatusMessage) {
		this.dashboardStatusMessage = dashboardStatusMessage;
	}

	public DateTime getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(DateTime loginTime) {
		this.loginTime = loginTime;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}
