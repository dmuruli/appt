package com.custanalytics.enotification.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.custanalytics.enotification.domain.Event;
import com.custanalytics.enotification.domain.Resource;
import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.service.AppointmentService;
import com.custanalytics.enotification.service.ProviderService;

@RestController
public class AppointmentController extends BaseController {

	@Autowired
	AppointmentService appointmentService;
	@Autowired
	ProviderService providerService;

	private static final Logger logger = Logger
			.getLogger(AppointmentController.class);

	@RequestMapping(method=RequestMethod.GET,  value = "/events")
	public  ResponseEntity<List<Event>>  getEvents(@RequestParam("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date start, 
			@RequestParam("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date end, @RequestParam("_")String endParam){
		logger.info("Searching for events with start of : " + start + " and end of: "+ end);
		List<Appointment>appointments =appointmentService.getActiveAppointmentsForLocation(-999L, 2L, start, end);
		List<Event>eventList = getEvents(appointments);
		loadSampleEvent(eventList);
		return new  ResponseEntity<List<Event>>(eventList,HttpStatus.OK);
	}
	
	private List<Event> getEvents(List<Appointment> appointments) {
		List<Event>eventList = new ArrayList<Event>();
		for(Appointment a:appointments){
			Event e = new Event();
			e.setId(a.getId());
			e.setResourceId(a.getProvider().getId());
			e.setTitle(a.getProvider().getFirstName() + " " + a.getProvider().getLastName());
			e.setStart(new DateTime(a.getStartTime()).toString());
			e.setEnd(new DateTime(a.getEndTime()).toString());
			eventList.add(e);
		}
		return eventList;
	}

	@RequestMapping(method=RequestMethod.GET,  value = "/resources")
	public  ResponseEntity<List<Resource>>  getResources(){
		List<Provider>providers =providerService.getProvidersByAccountByLocation(1L, 2L);
		List<Resource>resources =getResources(providers);
		return new  ResponseEntity<List<Resource>>(resources,HttpStatus.OK);
	}
	
	private void loadSampleEvent(List<Event> events) {
			
		DateTimeFormatter fmt = org.joda.time.format.DateTimeFormat.forPattern("yyyy-dd-mm'T'HH:mm:ss");
		
		Event event = new Event();
		event.setId(2);
		event.setResourceId(22);
		DateTime startTime = new DateTime();
		
		startTime.plusHours(1);
		DateTime endTime = new DateTime();
		endTime.plusHours(2);
		
		event.setStart(startTime.toString());
		event.setEnd(endTime.toString());
		event.setTitle("Test Doctors Appointment");
		events.add(event);	
	}

	private List<Resource> getResources(List<Provider> providers) {
		List<Resource> resources = new ArrayList<Resource>();
		for(Provider p: providers){
			Resource r = new Resource();
			r.setId(p.getId());
			r.setTitle("Dr." + p.getFirstName() + " " + p.getLastName());
			r.setEventColor("Blue");
			resources.add(r);
		}
		return resources;
	}


}
