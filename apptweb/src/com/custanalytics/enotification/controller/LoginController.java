package com.custanalytics.enotification.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.custanalytics.enotification.service.AccountService;

@Controller

public class LoginController extends BaseController {
	@Autowired
	AccountService accountService;
	private static final Logger logger = Logger
			.getLogger(LoginController.class);
	@RequestMapping(value = "/login")
	public ModelAndView login() {
		return new ModelAndView("calendarview");
	}

}
