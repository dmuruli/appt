package com.custanalytics.enotification.controller;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.SessionAttributes;

import com.custanalytics.enotification.domain.UserInfo;

@SessionAttributes("userInfo")
public class BaseController {
	
	protected void storeUserData(HttpSession httpSession, UserInfo userInfo){
		httpSession.setAttribute("userInfo", userInfo);
	}
	
	protected UserInfo getUserInfo(HttpSession httpSession){
		return (UserInfo)httpSession.getAttribute("userInfo");
	}

}
