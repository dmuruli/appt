package com.custanalytics.enotification.respository;

import java.util.List;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import com.custanalytics.enotification.entity.Reminder;


public interface ReminderRepository   extends JpaRepository<Reminder, Long>{
	public List<Reminder> findByAppointmentId(Long appointmentId);
	@Lock(LockModeType.PESSIMISTIC_READ)
	@Query("SELECT r FROM Reminder r WHERE ACCOUNT_ID =?1 AND TYPE=0 AND SENT=FALSE")
	public List<Reminder> findEmailRemindersByAccountId(Long accountId);
	@Lock(LockModeType.PESSIMISTIC_READ)
	@Query("SELECT r FROM Reminder r WHERE ACCOUNT_ID =?1 AND TYPE=1 AND SENT=FALSE")
	public List<Reminder>findSMSReminderByAccountId(Long accountId);
	
	@Query("SELECT r FROM Reminder r WHERE ACCOUNT_ID =?1 AND TYPE=?2 AND SENT=FALSE")
	public List<Reminder> findEmailRemindersByAccountIdAndReminderType(Long accountId, Integer reminderType);
	
	

}
