package com.custanalytics.enotification.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.custanalytics.enotification.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
	

}
