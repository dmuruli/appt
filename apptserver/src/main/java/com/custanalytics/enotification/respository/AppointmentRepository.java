package com.custanalytics.enotification.respository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.custanalytics.enotification.entity.Appointment;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

	public List<Appointment> findByLocationId(Long locationId);

	public List<Appointment> findByAccountId(Long locationId);

	@Query("select a from Appointment a where a.cancelled=false and a.accountId=?1")
	public List<Appointment> findActiveAppointmentsByAccountId(Long accountId);

	@Query("select a from Appointment a where a.id=?1")
	public Appointment findById(Long Id);
	
	@Modifying
	@Transactional
	@Query("update  Appointment  set cancelled=true where id=?1")
	public void cancelAppointment(long appointmentId);
	
	@Query("select a from Appointment a where a.cancelled=false and a.accountId=:accountId"
			+ " and a.patient.smsNotification=true and a.startTime >:cutoffDate")
	public List<Appointment> findSmsAppointments( @Param("accountId") Long accountId, @Param("cutoffDate") Date cutoffDate);
	
	@Query("select a from Appointment a where a.cancelled=false and a.accountId=:accountId"
			+ " and a.patient.emailNotification=true and a.startTime >:cutoffDate")
	public List<Appointment> findEmailAppointments( @Param("accountId") Long accountId, @Param("cutoffDate") Date cutoffDate);
	
	@Query("select a from Appointment a where a.cancelled=false and a.accountId=:accountId and  a.location.id=:locationId and a.provider.id IN (:providerIds) and  "
			+ " a.startTime >=:startTime and a.endTime <=:endTime ")
	public List<Appointment> findActiveAppointmentsByProviderLocationForRange(
			@Param("accountId")Long accountId, @Param("locationId") Long locationId, @Param("providerIds")List<Long> providerIds, @Param("startTime")Date startTime,
			@Param("endTime")Date endTime);

}
