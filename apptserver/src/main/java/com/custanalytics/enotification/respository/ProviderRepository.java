package com.custanalytics.enotification.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custanalytics.enotification.entity.Provider;

public interface ProviderRepository extends JpaRepository<Provider, Long> {

	Provider findByUserNameAndPassword(String userName, String password);
	List<Provider> findByAccountId(Long accountId);
}
