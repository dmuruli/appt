package com.custanalytics.enotification.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.custanalytics.enotification.entity.Patient;

public interface PatientRepository extends JpaRepository<Patient,Long>{

}
