package com.custanalytics.enotification.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.custanalytics.enotification.entity.Location;
import com.custanalytics.enotification.entity.Provider;

public interface LocationRepository extends JpaRepository<Location, Long> {
	@Query("SELECT DISTINCT  l.providers FROM Location l,  IN(l.providers) lp  where l.id = :locationId and lp.accountId=:accountId")
	public List<Provider> getProvidersForLocation(
			@Param("accountId") Long accountId, 
			@Param("locationId") Long locationId);

}
