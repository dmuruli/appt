package com.custanalytics.enotification.service;

import java.util.List;

import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Reminder;

public interface ReminderService {

	public Reminder createReminderByAppt(Appointment appointment);
	public List<Reminder> getReminderByAppointmentId(Long appointmentId);
	public void sendNotification(List<Appointment> appointments);
	public void sendNotification(Appointment appointment);
	public List<Reminder> getHtmlRemindersByAccount(Long accountId);
	public List<Reminder> getSMSRemindersByAccount(Long accountId);
	public void updateReminder(Reminder reminder);
}
