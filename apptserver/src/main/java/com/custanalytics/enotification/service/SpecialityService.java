package com.custanalytics.enotification.service;

import java.util.List;

import com.custanalytics.enotification.entity.Speciality;

public interface SpecialityService {

	public List<Speciality>getSpecialityByLocation(Long accountId, Long locationId);

}