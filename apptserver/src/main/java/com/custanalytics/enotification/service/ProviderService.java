package com.custanalytics.enotification.service;

import java.util.List;

import com.custanalytics.enotification.entity.Provider;

public interface ProviderService {

	public Provider createProvider(Provider provider);

	public Provider getProvider(Long providerId);

	public List<Provider> getProvidersForAccount(Long accountId);

	public List<Provider> getProvidersByAccountByLocation(Long accountId,
			Long locationId);

	public List<Provider> getProvidersByAccountByLocationBySpeciality(
			Long accountId, Long locationId, Long specialityId);

}
