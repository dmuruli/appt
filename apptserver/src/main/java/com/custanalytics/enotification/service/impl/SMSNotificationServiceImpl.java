package com.custanalytics.enotification.service.impl;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.custanalytics.enotification.rest.entity.Notification;
import com.custanalytics.enotification.rest.entity.SmsNotificationResponse;
import com.custanalytics.enotification.service.NotificationService;
import com.custanalytics.enotification.service.SMSNotificationService;
import com.custanalytics.enotification.service.config.SMSConfig;
@Service
public class SMSNotificationServiceImpl implements SMSNotificationService {
	private static final Logger logger = Logger
			.getLogger(NotificationService.class);
	RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	SMSConfig smsConfig;

	@Override
	public boolean smsStatusCheck() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sendSMSNotification(Notification notification) {
		return smsNotification(notification.getPlainText(), notification.getPhoneNumber());
	}

	private boolean smsNotification(String msg, String phoneNumber) {

		HttpEntity<Object> request = createRequestEntity(msg, phoneNumber);
		logger.info("Request: " + request.toString());
		ResponseEntity<SmsNotificationResponse> response = restTemplate
				.exchange(smsConfig.getTwilloMailurl(), HttpMethod.POST,
						request, SmsNotificationResponse.class);
		logger.info("Found the response: " + response.toString());
		return (response.getStatusCode()==HttpStatus.CREATED);
	}

	private HttpEntity<Object> createRequestEntity(String msg, String smsPhoneNumber) {
		final String plainCreds = smsConfig.getTwilloAccountSid() + ":" + smsConfig.getTwilloAuthToken();
		final String base64Creds = new String(Base64.encodeBase64(plainCreds
				.getBytes()));

		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
		body.add("To", smsPhoneNumber);
		body.add("From", smsConfig.getSenderPhoneNumber());
		body.add("Body", msg);
		body.add("Format", "JSON");

		return new HttpEntity<Object>(body, headers);
	}

}
