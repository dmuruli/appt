package com.custanalytics.enotification.service.config;


public class SMSConfig {
	
	private String twilloAuthToken;
	private String twilloAccountSid;
	private String twilloMailurl;
	private String senderPhoneNumber;
	
	public String getTwilloAuthToken() {
		return twilloAuthToken;
	}
	public void setTwilloAuthToken(String twilloAuthToken) {
		this.twilloAuthToken = twilloAuthToken;
	}
	public String getTwilloAccountSid() {
		return twilloAccountSid;
	}
	public void setTwilloAccountSid(String twilloAccountSid) {
		this.twilloAccountSid = twilloAccountSid;
	}
	public String getTwilloMailurl() {
		return twilloMailurl;
	}
	public void setTwilloMailurl(String twilloMailurl) {
		this.twilloMailurl = twilloMailurl;
	}
	public String getSenderPhoneNumber() {
		return senderPhoneNumber;
	}
	public void setSenderPhoneNumber(String senderPhoneNumber) {
		this.senderPhoneNumber = senderPhoneNumber;
	}
	
	

}
