package com.custanalytics.enotification.service;

import com.custanalytics.enotification.rest.entity.Notification;

public interface EmailNotificationService {

	public boolean emailServiceStatusCheck();

	public boolean sendMailNotification(Notification mailNotification);

}
