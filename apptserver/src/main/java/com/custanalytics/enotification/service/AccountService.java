package com.custanalytics.enotification.service;

import java.util.List;

import com.custanalytics.enotification.entity.Account;
import com.custanalytics.enotification.entity.Location;
import com.custanalytics.enotification.entity.Patient;
import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.domain.UserInfo;

public interface AccountService {

	public UserInfo login(String userName, String password);

	public Provider addEmployee(Provider employee, Account account);

	public List<Provider> getEmployeesByLocation(Long accountId, Long locationId);

	public Provider addProvider(Provider doctor, Long accountId);

	public Provider findProviderById(Long testaccountId);

	public Provider updateProvider(Provider provider);

	public List<Provider> findProvidersByAccount(Long accountId);
	
	public List<Patient> findPatientByAccount(Long accountId);

	public List<Location> findLocationsByAccount(Long accountId);

	public Account getAccount(Long accountId);

}
