package com.custanalytics.enotification.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.custanalytics.enotification.entity.Account;
import com.custanalytics.enotification.entity.Reminder;
import com.custanalytics.enotification.respository.AccountRepository;
import com.custanalytics.enotification.rest.entity.Notification;
import com.custanalytics.enotification.service.EmailNotificationService;
import com.custanalytics.enotification.service.NotificationService;
import com.custanalytics.enotification.service.ReminderService;
import com.custanalytics.enotification.service.SMSNotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {
	@Autowired
	ReminderService reminderService;
	@Autowired
	EmailNotificationService emailNotificationService;
	@Autowired
	SMSNotificationService smsNotificationService;
	@Autowired
	AccountRepository accountRepository;

	private static final Logger logger = Logger
			.getLogger(NotificationService.class);
	RestTemplate restTemplate = new RestTemplate();

	/**
	 * Look at moving the data out of the service and putting into configuration
	 * file
	 */

	@Override
	public boolean smsNotification(String msg, String phoneNumber) {

	/*	HttpEntity<Object> request = createRequestEntity(msg);
		logger.info("Request: " + request.toString());
		ResponseEntity<SmsNotificationResponse> response = restTemplate
				.exchange(TWILIO_BASE_URL + SEND_SMS_URL, HttpMethod.POST,
						request, SmsNotificationResponse.class);
		logger.info("Found the response: " + response.toString());*/
		return false;
	}

	@Override
	public boolean vmNotification(String msg) {
		return false;
	}

	private List<HttpMessageConverter<?>> createMessageConverter() {
		List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
		converters.add(new MappingJackson2HttpMessageConverter());
		return converters;
	}

	private HttpEntity<Object> createRequestEntity(String msg) {
	/*	final String plainCreds = ACCOUNT_SID + ":" + AUTH_TOKEN;
		final String base64Creds = new String(Base64.encodeBase64(plainCreds
				.getBytes()));

		final HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);

		MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
		body.add("To", "+14154120269");
		body.add("From", "+14153002777");
		body.add("Body", msg);
		body.add("Format", "JSON");

		return new HttpEntity<Object>(body, headers);*/
		
		return null;
	}

	@Override
	public void sendAccountSMSNotifications(Long accountId) {	
		List<Reminder> reminders = reminderService
				.getSMSRemindersByAccount(accountId);
		for (Reminder reminder : reminders) {
			Notification mn = createMailNotification(reminder);
			boolean sent =smsNotificationService.sendSMSNotification(mn);
			if(sent){
				logger.debug("Mail Notification sent for appointmentId: "
						+ reminder.getAppointmentId() +" and reminderId: " +reminder.getId());
				reminder.setSent(true);
				reminder.setUpdatedDate(new Date());
				reminderService.updateReminder(reminder);
			}
		}
		
	}

	public void sendAccountHTMLNotifications(Long accountId) {
		List<Reminder> reminders = reminderService
				.getHtmlRemindersByAccount(accountId);
		for (Reminder reminder : reminders) {
			Notification mn = createMailNotification(reminder);
			boolean sent = emailNotificationService.sendMailNotification(mn);
			if (sent) {
				logger.debug("Mail Notification sent for appointmentId: "
						+ reminder.getAppointmentId() +" and reminderId: " +reminder.getId());
				reminder.setSent(true);
				reminder.setUpdatedDate(new Date());
				//reminderService.updateReminder(reminder);
			}
		}

	}

	private Notification createMailNotification(Reminder reminder) {
		DateTime apptTime = new DateTime(reminder.getAppointmentStartTime());
		DateTimeFormatter fmt = DateTimeFormat.forPattern("EE MMMM, dd, yyyy");
		String displayApptTime = apptTime.toString(fmt);
		String msg ="Your appointment with " + reminder.getProvider().getFirstName() +", " + reminder.getProvider().getLastName()+
				" is scheduled to occur on " +displayApptTime +"  Please"
				+ " call the office to confirm";
		Notification mn = new Notification();
		mn.setFrom("dmuruli@custanalytics.com");
		mn.setSubject("Therapy Appointment Reminder");
		mn.setPlainText(msg);
		mn.setPhoneNumber(reminder.getPhoneNumber());
		mn.setTo(reminder.getEmail());
		return mn;
	}

	@Override
	public void sendNotifications() {
		List<Account> allAccounts = accountRepository.findAll();
		for (Account account : allAccounts) {
			logger.debug("Sending out notifications for account :"
					+ account.getName());

			sendAccountHTMLNotifications(account.getId());
			sendAccountSMSNotifications(account.getId());
		}

	}

}
