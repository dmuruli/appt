package com.custanalytics.enotification.service;

import com.custanalytics.enotification.rest.entity.Notification;

public interface SMSNotificationService {
		
	public boolean smsStatusCheck();
	public boolean sendSMSNotification(Notification mailNotification);
}
