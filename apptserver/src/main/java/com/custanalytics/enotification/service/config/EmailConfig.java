package com.custanalytics.enotification.service.config;

public class EmailConfig {
	
	String apiKey;
	String endPoint;
	String testEndPoint;
	String sendGridSender;
	
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getTestEndPoint() {
		return testEndPoint;
	}
	public void setTestEndPoint(String testEndPoint) {
		this.testEndPoint = testEndPoint;
	}
	public String getSendGridSender() {
		return sendGridSender;
	}
	public void setSendGridSender(String sendGridSender) {
		this.sendGridSender = sendGridSender;
	}
	
}
