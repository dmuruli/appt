package com.custanalytics.enotification.service;

import java.util.Date;
import java.util.List;

import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Provider;

public interface AppointmentService {
	
	public Appointment createAppointment(Appointment appointment);
	public Appointment getAppointment(Long appointmentId);
	public void cancelAppointment(Long appointmentId);
	public Appointment updateAppointment(Appointment appointment);
	public List<Appointment> getAppointments(Long accountId);
	public List<Appointment>getActiveAppointments(Long accountId);
	public List<Appointment> getActiveAppointments(Long accountId,
			Long locationId, List<Long> providerIds, Date startTime,
			Date endTime);
	List<Appointment> getActiveAppointmentsForLocation(Long accountId,
			Long locationId, Date startTime,
			Date endTime);
	public List<Appointment>getActiveSmsAppointments(Long accountId);
	public List<Appointment>getActiveEmailAppointments(Long accountId);
	public List<Appointment>getNoReminderAppointmentsByAccount(Long accountId);
}
