package com.custanalytics.enotification.service.impl;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.custanalytics.enotification.rest.entity.Notification;
import com.custanalytics.enotification.rest.entity.MailResponse;
import com.custanalytics.enotification.service.EmailNotificationService;
import com.custanalytics.enotification.service.config.EmailConfig;

@Service
public class EmalNotificationServiceImpl implements EmailNotificationService {
	Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	EmailConfig emailConfig;
	RestTemplate restTemplate;
	MultiValueMap<String, String> headerParams;
	HttpHeaders headers;

	private RestTemplate getRestTemplate() {
		if (restTemplate == null) {
			restTemplate = new RestTemplate();
			headerParams = new LinkedMultiValueMap<String, String>();
			String authKey = "Bearer " + emailConfig.getApiKey();

			headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.set("Authorization", authKey);
			restTemplate.getMessageConverters().add(
					new MappingJackson2HttpMessageConverter());
			restTemplate.getMessageConverters().add(
					new StringHttpMessageConverter());
		}
		return restTemplate;
	}

	@Override
	public boolean emailServiceStatusCheck() {
		RestTemplate restTemplate = getRestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters",
				headers);
		ResponseEntity<String> result =restTemplate.exchange(emailConfig.getTestEndPoint(),
				HttpMethod.GET, entity, String.class);
		logger.info("Footer settings: " + result);
		return (result != null);
	}

	@Override
	public boolean sendMailNotification(Notification mailNotification) {
		String result = null;
		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			map.add("from", emailConfig.getSendGridSender());
			map.add("to", mailNotification.getTo());
			map.add("subject", mailNotification.getSubject());
			map.add("text", mailNotification.getPlainText());
			String authKey = "Bearer " + emailConfig.getApiKey();
			map.add("api_key", authKey);
			RestTemplate restTemplate = getRestTemplate();
			HttpEntity<?> httpEntity = new HttpEntity<Object>(map, headers);
			ResponseEntity<MailResponse> response = restTemplate.exchange(
					emailConfig.getEndPoint(), HttpMethod.POST, httpEntity,
					MailResponse.class);
			result = response.getBody().getMessage();
			logger.info("Result of sending email: " + result);
		} catch (HttpClientErrorException e) {
			logger.error("error:  " + e.getResponseBodyAsString());
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage());
		}
		return (result != null);
	}

}
