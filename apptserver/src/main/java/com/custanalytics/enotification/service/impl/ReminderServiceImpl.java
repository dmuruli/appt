package com.custanalytics.enotification.service.impl;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Patient;
import com.custanalytics.enotification.entity.Reminder;
import com.custanalytics.enotification.respository.ReminderEnum;
import com.custanalytics.enotification.respository.ReminderRepository;
import com.custanalytics.enotification.service.ReminderService;

@Service
public class ReminderServiceImpl implements ReminderService {
	@Autowired
	ReminderRepository reminderRepository;

	@Override
	public Reminder createReminderByAppt(Appointment appointment) {
		Reminder reminder = new Reminder();
		reminder.setProvider(appointment.getProvider());
		reminder.setAppointmentId(appointment.getId());
		/**
		 * Change reminder type to a numeric value in patient, so can be in
		 * synch. Put definition in an enum 1-BOTH 2-SMS 3-EMAIL
		 */
		Patient patient = appointment.getPatient();
		if (patient.isEmailNotification() && patient.isSmsNotification()) {
			reminder.setType(1);
		} else if (patient.isEmailNotification()
				&& !patient.isSmsNotification()) {
			reminder.setType(2);
		} else if (!patient.isEmailNotification()
				&& patient.isSmsNotification()) {
			reminder.setType(3);
		}
		DateTime appointmentDateTime = new DateTime(appointment.getStartTime());

		reminder.setCuttoffTime(appointmentDateTime.minusHours(24).toDate());
		reminderRepository.save(reminder);
		return reminder;

	}

	@Override
	public List<Reminder> getReminderByAppointmentId(Long appointmentId) {
		return reminderRepository.findByAppointmentId(appointmentId);
	}

	@Override
	public void sendNotification(List<Appointment> appointments) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendNotification(Appointment appointment) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Reminder> getHtmlRemindersByAccount(Long accountId) {
		return reminderRepository.findEmailRemindersByAccountId(accountId);
	}

	@Override
	public List<Reminder> getSMSRemindersByAccount(Long accountId) {
		return reminderRepository.findEmailRemindersByAccountIdAndReminderType(accountId, ReminderEnum.SMS.ordinal());
	}

	@Override
	public void updateReminder(Reminder reminder) {
		reminderRepository.saveAndFlush(reminder);
		
	}
	

}
