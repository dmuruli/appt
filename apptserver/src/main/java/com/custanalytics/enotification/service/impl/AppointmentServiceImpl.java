package com.custanalytics.enotification.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.respository.AppointmentRepository;
import com.custanalytics.enotification.respository.LocationRepository;
import com.custanalytics.enotification.respository.PatientRepository;
import com.custanalytics.enotification.service.AppointmentService;
import com.custanalytics.enotification.service.ReminderService;

@Service
public class AppointmentServiceImpl implements AppointmentService {
	@Autowired
	AppointmentRepository appointmentRepository;
	@Autowired
	PatientRepository patientRepository;
	@Autowired
	ReminderService reminderService;
	@Autowired
	LocationRepository locationRepository;

	@Override
	@Transactional
	public Appointment createAppointment(Appointment appointment) {
		Appointment createdAppointment = appointmentRepository
				.save(appointment);
		reminderService.createReminderByAppt(createdAppointment);
		return createdAppointment;
	}

	@Override
	public void cancelAppointment(Long appointmentId) {
		appointmentRepository.cancelAppointment(appointmentId);
	}

	@Override
	@Transactional
	public Appointment updateAppointment(Appointment appointment) {
		return appointmentRepository.saveAndFlush(appointment);
	}

	@Override
	public List<Appointment> getAppointments(Long accountId) {
		return appointmentRepository.findAll();
	}

	@Override
	public List<Appointment> getActiveAppointments(Long accountId) {
		return appointmentRepository
				.findActiveAppointmentsByAccountId(accountId);
	}

	@Override
	public Appointment getAppointment(Long appointmentId) {
		Appointment appointment = appointmentRepository.findById(appointmentId);
		return appointment;
	}

	@Override
	public List<Appointment> getActiveSmsAppointments(Long accountId) {

		List<Appointment> smsAppointments = new ArrayList<Appointment>();

		DateTime cutOffTime = new DateTime();
		cutOffTime.plusHours(24);
		smsAppointments = appointmentRepository.findSmsAppointments(accountId,
				new Date(cutOffTime.getMillis()));
		return smsAppointments;
	}

	@Override
	public List<Appointment> getActiveEmailAppointments(Long accountId) {
		List<Appointment> emailAppointments = new ArrayList<Appointment>();
		DateTime cutOffTime = new DateTime();
		cutOffTime.plusHours(24);
		emailAppointments = appointmentRepository.findEmailAppointments(
				accountId, new Date(cutOffTime.getMillis()));
		return emailAppointments;
	}

	@Override
	public List<Appointment> getNoReminderAppointmentsByAccount(Long accountId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Appointment> getActiveAppointments(Long accountId,
			Long locationId, List<Long> providerIds, Date startTime,
			Date endTime) {
		return appointmentRepository
				.findActiveAppointmentsByProviderLocationForRange(accountId,
						locationId, providerIds, startTime, endTime);
	}

	@Override
	public List<Appointment> getActiveAppointmentsForLocation(Long accountId,
			Long locationId, Date startTime, Date endTime) {
		List<Appointment> appointments = new ArrayList<Appointment>();
		List<Provider> providers = locationRepository.getProvidersForLocation(
				accountId, locationId);
		List<Long> providerIds = getProviderIds(providers);
		if(providers.size()>0){
			appointments = getActiveAppointments(accountId,
					locationId, providerIds, startTime, endTime);
		}
		return appointments;
	}

	private List<Long> getProviderIds(List<Provider> providers) {
		List<Long> providerIds = new ArrayList<Long>();
		for (Provider p : providers) {
			providerIds.add(p.getId());
		}
		return providerIds;
	}

}
