package com.custanalytics.enotification.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.respository.LocationRepository;
import com.custanalytics.enotification.respository.ProviderRepository;
import com.custanalytics.enotification.service.ProviderService;
@Service
public class ProviderServiceImpl implements ProviderService {
	@Autowired
	ProviderRepository providerRepository;
	@Autowired
	LocationRepository locationRepository;

	@Override
	public Provider createProvider(Provider provider) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Provider getProvider(Long providerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Provider> getProvidersForAccount(Long accountId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Provider> getProvidersByAccountByLocation(Long accountId,
			Long locationId) {

		return locationRepository.getProvidersForLocation(
				accountId, locationId);
	}

	@Override
	public List<Provider> getProvidersByAccountByLocationBySpeciality(
			Long accountId, Long locationId, Long specialityId) {
		List<Provider> providers = new ArrayList<Provider>();
		return providers;
	}

}
