package com.custanalytics.enotification.service;

import java.util.List;
import com.custanalytics.enotification.entity.Patient;

public interface PatientService {

	public Patient addPatient(Patient patient, Long accountId);
	public boolean deactivatePatient(Long patientId);
	public Patient savePatient(Patient patient);
	public List<Patient>getPatientsByAccount(Long accountId);
	public List<Patient>getPatientsByProvider(Long employeeId);
	
}
