package com.custanalytics.enotification.service;


public interface NotificationService {
	
	public boolean smsNotification(String msg, String phoneNumber);
	
	public boolean vmNotification(String msg);

	public void sendAccountSMSNotifications(Long accountId);
	
	public void sendAccountHTMLNotifications(Long accountId);
	
	public void sendNotifications();
	

}
