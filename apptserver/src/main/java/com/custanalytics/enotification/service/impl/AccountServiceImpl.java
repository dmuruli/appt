package com.custanalytics.enotification.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.custanalytics.enotification.entity.Account;
import com.custanalytics.enotification.entity.Location;
import com.custanalytics.enotification.entity.Patient;
import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.entity.RoleType;
import com.custanalytics.enotification.respository.AccountRepository;
import com.custanalytics.enotification.respository.PatientRepository;
import com.custanalytics.enotification.respository.ProviderRepository;
import com.custanalytics.enotification.service.AccountService;
import com.custanalytics.enotification.domain.UserInfo;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	ProviderRepository providerRepository;
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	PatientRepository patientRepository;

	@Override
	public UserInfo login(String userName, String password) {
		UserInfo userInfo = null;
		Account account = null;

		Provider provider = providerRepository.findByUserNameAndPassword(
				userName, password);
		if (provider != null) {
			userInfo = new UserInfo();
			userInfo.setProvider(provider);
			account = accountRepository.findOne(provider.getAccountId());
			userInfo.setAccount(account);
		}
		return userInfo;
	}

	public Provider addEmployee(Provider employee, Account account) {
		Provider newEmployee = null;
		return newEmployee;
	}

	@Override
	public List<Provider> getEmployeesByLocation(Long accountId, Long locationId) {
		return null;
	}

	@Override
	public Provider addProvider(Provider provider, Long accountId) {
		Provider createdProvider = null;
		provider.setRoleId(new Long(RoleType.PROVIDER.ordinal()));
		provider.setAccountId(accountId);
		createdProvider = providerRepository.saveAndFlush(provider);
		return createdProvider;
	}

	@Override
	public Provider findProviderById(Long employeeId) {
		return providerRepository.findOne(employeeId);
	}

	@Override
	public Provider updateProvider(Provider provider) {
		return providerRepository.saveAndFlush(provider);
	}

	@Override
	public List<Provider> findProvidersByAccount(Long accountId) {
		return providerRepository.findByAccountId(accountId);
	}

	@Override
	@Transactional
	public Account getAccount(Long accountId) {
		Account account = accountRepository.findOne(accountId);
		return account;
	}

	@Override
	public List<Location> findLocationsByAccount(Long accountId) {
		List<Location> locations = new ArrayList<Location>();
		// locations=accountRepository.findAccountLocations(accountId);
		return locations;
	}

	@Override
	public List<Patient> findPatientByAccount(Long accountId) {
		return null;
	}

}
