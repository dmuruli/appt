package com.custanalytics.enotification.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;


import com.custanalytics.enotification.service.config.EmailConfig;
import com.custanalytics.enotification.service.config.SMSConfig;
@Configuration
@EnableScheduling
@PropertySource(value = "classpath:/app.properties", ignoreResourceNotFound = true)
@EnableJpaRepositories(basePackages = { "com.custanalytics.enotification.respository" })
@ComponentScan(basePackages = { "com.custanalytics.enotification.job",
		"com.custanalytics.enotification.service" })
public class AppConfig {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Value("${sendgrid.apikey}")
	private String sendGridApiKey;
	@Value("${sendgrid.enpoint}")
	private String sendGridEndPoint;
	@Value("${sendgrid.test.endpoint}")
	private String sendGridTestEndPoint;
	@Value("${sendgrid.sender}")
	private String sendGridSender;
	@Value("${DB_USERNAME}")
	private String dbUserName;
	@Value("${DB_PASSWORD}")
	private String dbPassword;
	@Value("${DB_URL}")
	private String dbUrl;
	@Value("${DB_CLASSNAME}")
	private String driverClass;
	
	@Value("${twillo.authtoken}")
	private String twilloAuthToken;
	@Value("${twillo.accountsid}")
	private String twilloAccountSid;
	@Value("${twillo.mailurl}")
	private String twilloMailUrl;
	@Value("${twillo.senderNumber}")
	private String senderPhoneNumber;
	@Value("${notification.run.status}")
	private String notificationRunStatus;

	@Bean
	public EmailConfig emailConfig() {
		EmailConfig emailConfig = new EmailConfig();
		emailConfig.setApiKey(sendGridApiKey);
		emailConfig.setEndPoint(sendGridEndPoint);
		emailConfig.setTestEndPoint(sendGridTestEndPoint);
		emailConfig.setSendGridSender(sendGridSender);
		return emailConfig;
	}
	@Bean
	public SMSConfig smsConfig(){
		SMSConfig smsConfig = new SMSConfig();
		smsConfig.setTwilloAccountSid(twilloAccountSid);
		smsConfig.setTwilloAuthToken(twilloAuthToken);
		smsConfig.setTwilloMailurl(twilloMailUrl);
		smsConfig.setSenderPhoneNumber(senderPhoneNumber);
		return smsConfig;
	}

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClass);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUserName);
		dataSource.setPassword(dbPassword);
		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties prop = new Properties();

		prop.put("hibernate.format_sql", "true");
		prop.put("hibernate.show_sql", "true");
		prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

		prop.put("hibernate.c3p0.min_size", "5");
		prop.put("hibernate.c3p0.max_size", "10");
		prop.put("hibernate.c3p0.max_statements", "50");
		prop.put("hibernate.c3p0.idle_test_period", "3000");

		return prop;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(getDataSource());
		em.setPackagesToScan(new String[] { "com.custanalytics.enotification.entity" });

		JpaVendorAdapter vendorAdaptor = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdaptor);
		em.setJpaProperties(getHibernateProperties());
		return em;
	}

	@Bean
	public PlatformTransactionManager transactionManager(
			EntityManagerFactory emf) {
		JpaTransactionManager txMgr = new JpaTransactionManager();
		txMgr.setEntityManagerFactory(emf);
		return txMgr;
	}

	@Bean(name = "persistenceExceptionTranslationPostProcessor")
	public PersistenceExceptionTranslationPostProcessor getExceptionProcessor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

}
