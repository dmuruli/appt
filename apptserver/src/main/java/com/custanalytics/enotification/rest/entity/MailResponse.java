package com.custanalytics.enotification.rest.entity;

import java.io.Serializable;

public class MailResponse implements Serializable {
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
