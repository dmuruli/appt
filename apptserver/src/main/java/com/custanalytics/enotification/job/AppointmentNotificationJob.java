package com.custanalytics.enotification.job;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.custanalytics.enotification.service.NotificationService;

@Service("appointmentNotificationJob")
public class AppointmentNotificationJob {

	@Autowired
	NotificationService notificationService;
	private static final Logger logger = Logger
			.getLogger(AppointmentNotificationJob.class);
	
	@Scheduled(fixedDelay=10000)
	public void execute()  {
		DateTime now = new DateTime();
		logger.debug("Sending out appointment notifications--"+ now);
		//notificationService.sendNotifications();	
	}

}
