package com.custanalytics.enotification.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "REMINDER")
public class Reminder implements Serializable {
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	Long id;
	@Column(name = "APPOINTMENT_ID")
	Long appointmentId;
	@Column(name = "APPT_START_TIME")
	Date appointmentStartTime;
	@Column(name = "SENT")
	Boolean sent;
	@Column(name = "CUTTOFF_TIME")
	Date cuttoffTime;
	@Column(name = "PATIENT_EMAIL")
	String email;
	@Column(name = "PATIENT_PHONE_NO")
	String phoneNumber;
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "PROVIDER_ID")
	Provider provider;
	@Column(name = "SUCCESS")
	Boolean succcess;
	@Column(name = "FAILURE_MESSAGE")
	String failureMessage;
	@Column(name = "TYPE")
	Integer type;
	@Column(name = "CREATED_BY")
	Date createdBy;
	@Column(name = "UPDATED_DATE")
	Date updatedDate;
	@Version
	long version;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}
	public Boolean getSent() {
		return sent;
	}
	public void setSent(Boolean sent) {
		this.sent = sent;
	}
	public Date getCuttoffTime() {
		return cuttoffTime;
	}
	public void setCuttoffTime(Date cuttoffTime) {
		this.cuttoffTime = cuttoffTime;
	}
	public Boolean getSucccess() {
		return succcess;
	}
	public void setSucccess(Boolean succcess) {
		this.succcess = succcess;
	}
	public String getFailureMessage() {
		return failureMessage;
	}
	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Date getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Date createdBy) {
		this.createdBy = createdBy;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Provider getProvider() {
		return provider;
	}
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	public Date getAppointmentStartTime() {
		return appointmentStartTime;
	}
	public void setAppointmentStartTime(Date appointmentStartTime) {
		this.appointmentStartTime = appointmentStartTime;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	
	
}
	