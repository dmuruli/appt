package com.custanalytics.enotification.service.test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import com.custanalytics.enotification.config.AppConfig;


@ContextConfiguration(classes = { AppConfig.class })
public class BaseTest extends AbstractTestNGSpringContextTests {

}
