package com.custanalytics.enotification.service.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Patient;
import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.entity.Reminder;
import com.custanalytics.enotification.service.AppointmentService;
import com.custanalytics.enotification.service.ReminderService;

public class AppointmentServiceTest extends BaseTest {
	@Autowired
	AppointmentService appointmentService;
	@Autowired
	ReminderService reminderService;

	@Test
	public void testFindAllAppointmentsByAccount() {

		Long testId = 1L;
		List<Appointment> appointments = appointmentService
				.getActiveAppointments(testId);
		Assert.assertNotNull(appointments,
				"Could not find appointments for account Id: " + testId
						+ " returned null");
		Assert.assertTrue(appointments.size() > 0,
				"Could not find appointments for account Id: " + testId);
	}

	public void testFindAppointment() {
		Long testAppointmentId = 1L;
		Appointment foundAppointment = appointmentService
				.getAppointment(testAppointmentId);

		Assert.assertNotNull(foundAppointment);
		Assert.assertEquals(!foundAppointment.isCancelled(),
				"Appointment with id: " + testAppointmentId
						+ " is cancelled, but should not be cancelled");
	}

	@Test
	public void testFindAppointmentsByAccountId() {
		Long accountId = 1L;
		List<Appointment> appointments = appointmentService
				.getAppointments(accountId);

		Assert.assertNotNull(appointments,
				"Could not find appointments for account Id: " + accountId);
		Assert.assertNotNull(appointmentService);
	}

	@Test
	public void testFindActiveAppointments() {
		Long testAccountId = 1L;
		List<Appointment> appointments = appointmentService
				.getActiveAppointments(testAccountId);
		Assert.assertNotNull(appointments,
				"Could not find active appointments for account Id: "
						+ testAccountId);
		Assert.assertTrue(appointments.size() == 1,
				"Found more than the expected 1 active appointment");
		Assert.assertNotNull(appointments.get(0).getLocation(),
				"Appointment Locaiton is null");
	}

	@Test
	public void testRetrieveAppointmentData() {
		Long testAccountId = 1L;
		Appointment appointment = appointmentService
				.getAppointment(testAccountId);
		Long accountId = appointment.getAccountId();
		Assert.assertNotNull(accountId);
		Assert.assertTrue(accountId == 1,
				"The wrong accountId was retrieved was expecting: " + 1
						+ "but found: " + accountId);

	}

	@Test
	public void testModifyAppointment() {
		Long testAccountId = 1L;
		Appointment appointment = appointmentService
				.getAppointment(testAccountId);
		Date startDate = new Date();
		appointment.setStartTime(startDate);
		Appointment updatedAppointment = appointmentService
				.updateAppointment(appointment);
		Assert.assertEquals(updatedAppointment.getStartTime().getTime(),
				startDate.getTime());
	}

	@Test
	public void testCreateAppointment() {
		Appointment testAppointment = createAppointment();
		Appointment createdAppointment = appointmentService
				.createAppointment(testAppointment);
		Assert.assertNotNull(createdAppointment.getId(),
				"Not able to create appointment, id is null");
		List<Reminder> apptReminders = reminderService
				.getReminderByAppointmentId(createdAppointment.getId());
		Assert.assertNotNull(apptReminders);
		Assert.assertTrue(
				apptReminders.get(0).getAppointmentId() == createdAppointment.getId(),
				"Reminder id: " + apptReminders.get(0).getAppointmentId() + "is not"
						+ "	the same as the createdAppointment id: "
						+ createdAppointment.getId());

	}

	@Test
	public void testCancelAppointment() {

		Appointment testCancelAppointment = createAppointment();
		Appointment createdAppointment = appointmentService
				.createAppointment(testCancelAppointment);
		appointmentService.cancelAppointment(createdAppointment.getId());
		Appointment cancelledAppointment = appointmentService
				.getAppointment(createdAppointment.getId());
		Assert.assertNotNull(cancelledAppointment);
		Assert.assertTrue(
				cancelledAppointment.isCancelled(),
				"appointment with appointmentId: "
						+ cancelledAppointment.getId() + " was not cancelled");
	}

	@Test
	public void testGetSmsNotificationAppointments() {

		List<Appointment> appointments = appointmentService
				.getActiveSmsAppointments(-999L);

		Assert.assertNotNull(appointments);
		Assert.assertTrue(appointments.size() == 2);
	}

	@Test
	public void testGetEmailNotificationAppointments() {
		List<Appointment> appointments = appointmentService
				.getActiveEmailAppointments(-999L);

		Assert.assertNotNull(appointments);
		Assert.assertTrue(appointments.size() == 2);
	}
	
	@Test
	public void testGetAppointmentsByLocationAndProvidersAndDateRange(){
		// requires inserting or updating test data, prior to run
		// if testing for current day appointments
		Long accountId =-999L;
		Long locationId =2L;
		DateTime startTime = new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
		DateTime endTime = new DateTime().withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
		List<Long> testProviderIds = getTestProviderIds();
		List<Appointment> appointments = appointmentService
	    .getActiveAppointments( accountId,
			 locationId, testProviderIds,  startTime.toDate(),
			 endTime.toDate());
		Assert.assertNotNull(appointments);
		System.out.println("No. of appointments: " + appointments.size());
		Assert.assertTrue(appointments.size()==3);
	}
	
	@Test
	public void testGetAppointsForDateRange(){
		DateTime startTime = new DateTime().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
		DateTime endTime = new DateTime().withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
		List<Appointment>appointments =appointmentService.getActiveAppointmentsForLocation(1L, 2L, startTime.toDate(), endTime.toDate());
		Assert.assertNotNull(appointments,"Could not find appointments for date range start: " +startTime +" ending: "+endTime);
	}

	private List<Long> getTestProviderIds() {
		List<Long> testProviderIds = new ArrayList<Long>();
		testProviderIds.add(1L);
		testProviderIds.add(3L);
		testProviderIds.add(4L);
		return testProviderIds;
	}

	private Appointment createAppointment() {
		Appointment testAppointment = new Appointment();
		testAppointment.setAccountId(1L);

		Patient newPatient = new Patient();
		newPatient.setId(1L);
		testAppointment.setPatient(newPatient);

		Provider newProvider = new Provider();
		newProvider.setId(1L);
		testAppointment.setProvider(newProvider);

		DateTime startTime = new DateTime();

		testAppointment.setStartTime(startTime.toDate());
		DateTime endTime = startTime.plusHours(1);
		testAppointment.setEndTime(endTime.toDate());
		return testAppointment;
	}

}
