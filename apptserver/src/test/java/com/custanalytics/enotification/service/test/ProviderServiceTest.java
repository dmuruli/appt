package com.custanalytics.enotification.service.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.service.ProviderService;

public class ProviderServiceTest extends BaseTest{
	
	@Autowired
	ProviderService providerService;
	
	@Test
	public void testFindTestProviders(){
		Long mountainViewLocation = 2L;
		List<Provider>providers =providerService.getProvidersByAccountByLocation(-999L, mountainViewLocation );
		Assert.assertTrue(providers.size()==3,"Did not return the expected no of providers (3)");
	}

}
