package com.custanalytics.enotification.service.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.custanalytics.enotification.domain.UserInfo;
import com.custanalytics.enotification.entity.Account;
import com.custanalytics.enotification.entity.Location;
import com.custanalytics.enotification.entity.Patient;
import com.custanalytics.enotification.entity.Provider;
import com.custanalytics.enotification.service.AccountService;

public class AccountServiceTest extends BaseTest {
	@Autowired
	AccountService accountService;

	@Test
	public void loginTest() {
		String testUserName = "mwelby";
		String testPassword = "password1";

		UserInfo userInfo = accountService.login(testUserName, testPassword);
		Assert.assertNotNull(userInfo, "login failed, employee object is null");
		Assert.assertNotNull(userInfo.getAccount(),
				"Not able to find account associated with testUser: "
						+ testUserName);
	}

	@Test
	public void addMedicalStaffTest() {

		Provider doctor = new Provider();
		doctor.setFirstName("Marcus");
		doctor.setLastName("Welby");
		doctor.setUserName("mwelby");

		doctor.setPassword("password_"
				+ String.valueOf((int) (Math.random() * 9000) + 1000));
		Long accountId = -999L;
		Provider savedDoctor = accountService.addProvider(doctor, accountId);
		Assert.assertNotNull(savedDoctor,
				"adding doctor failed, returned null object");
		Assert.assertTrue(savedDoctor.getId() != null,
				"adding doctor failed, id is null");
	}

	@Test
	public void modifyProviderTest() {

		String testUserName = "";
		String newPhoneNumber = "555-121-1212";

		Long testaccountId = 1L;
		Provider provider = accountService.findProviderById(testaccountId);

		provider.setTelephoneNumber(newPhoneNumber);
		Provider updatedProvider = accountService.updateProvider(provider);
		Assert.assertNotNull(updatedProvider.getTelephoneNumber(),
				"adding provider telephone number failed, number is null");
		Assert.assertTrue(updatedProvider.getTelephoneNumber()
				.equalsIgnoreCase(newPhoneNumber),
				"new Provider phone number is not");
	}

	@Test
	public void findProviderByAccount() {
		Long testAccountId = -999L;

		Provider provider1 = createTestProvider("Test User");

		accountService.addProvider(provider1, testAccountId);

		List<Provider> providers = accountService
				.findProvidersByAccount(testAccountId);

		Assert.assertNotNull(providers,
				"Not able to find providers by account, providers list is null");
		Assert.assertTrue(providers.size() > 0,
				"Failed to retrieve providers, provider list size is not greater than zero");
	}

	private Provider createTestProvider(String userName) {
		Provider provider = new Provider();

		provider.setUserName(userName);
		return provider;
	}

	@Test
	public void getAccountLocations() {
		Long accountId = 1L;
		Account testAccount = accountService.getAccount(accountId);
		Assert.assertNotNull(testAccount);
		List<Location> accountLocations = testAccount.getLocations();

		Assert.assertTrue(accountLocations.size() > 0,
				"Could not find account locations for acccountId: " + accountId);
	}

	@Test
	public void getAccountPatients() {
		Long accountId = 1L;
		Account testAccount = accountService.getAccount(accountId);
		Assert.assertNotNull(testAccount);

		List<Patient> accountPatients = testAccount.getPatients();
		Assert.assertTrue(accountPatients.size() == 4,
				"Could not find right no. of patients for  acccountId: " + accountId + " found " +accountPatients.size());

	}
	@Test
	public void getAccountProviders() {
		Long accountId = 1L;
		Account testAccount = accountService.getAccount(accountId);
		Assert.assertNotNull(testAccount);

		List<Provider> accountProviders = testAccount.getProviders();
		Assert.assertTrue(accountProviders.size() == 1,
				"Could not find right no. of providers for  acccountId: " + accountId + " found " +accountProviders.size());

	}

}
