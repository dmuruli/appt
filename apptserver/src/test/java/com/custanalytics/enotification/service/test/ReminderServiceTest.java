package com.custanalytics.enotification.service.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.custanalytics.enotification.entity.Appointment;
import com.custanalytics.enotification.entity.Reminder;
import com.custanalytics.enotification.service.AppointmentService;
import com.custanalytics.enotification.service.ReminderService;

public class ReminderServiceTest extends BaseTest {
	@Autowired
	ReminderService reminderService;
	@Autowired
	AppointmentService appointmentService;

	@Test
	public void createReminderTest() {
		Appointment appointment = createReminderTestAppointment();
		Reminder reminder = reminderService.createReminderByAppt(appointment);
		Assert.assertNotNull(reminder, "Reminder not created, reminder is null");
		Assert.assertTrue(reminder.getId() > 0,
				"Reminder Id not created, the reminder id is zero");
	}

	@Test
	public void retrieveReminderTest() {
		Long appointmentId = 1L;
		List<Reminder> reminders = reminderService
				.getReminderByAppointmentId(appointmentId);
		Assert.assertNotNull(reminders,
				"No reminder was not found for appointmentId: "
						+ appointmentId);
	}

	@Test
	public void findHtmlRemindersTest() {
		long testAccountId = 1;
		List<Reminder> htmlReminders = reminderService
				.getHtmlRemindersByAccount(testAccountId);
		Assert.assertNotNull(htmlReminders, "List of html reminders is null");
		Assert.assertTrue(htmlReminders.size() >= 1,
				"Did not find any html reminders");
	}

	@Test
	public void findSMSRemindersTest() {
		long testAccountId = 1;
		List<Reminder> smsReminders = reminderService
				.getSMSRemindersByAccount(testAccountId);
		Assert.assertNotNull(smsReminders, "List of sms reminders is null");
		Assert.assertTrue(smsReminders.size() >= 1,
				"Did not find any sms reminders");
	}

	/**
	 * @TODO, Load with test appointment created in test data sql, will need to
	 *        look into dbunit, a way to load test data by configuration.
	 * @return
	 */
	private Appointment createReminderTestAppointment() {
		Appointment appointment = new Appointment();
		appointment = appointmentService.getAppointment(7L);
		return appointment;
	}

}
