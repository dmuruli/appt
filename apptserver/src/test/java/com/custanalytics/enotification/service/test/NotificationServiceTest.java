package com.custanalytics.enotification.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import com.custanalytics.enotification.service.NotificationService;

public class NotificationServiceTest extends BaseTest {
	@Autowired
	NotificationService notificationService;

	@Test
	public void testSendNotificationMessage() {
		notificationService.smsNotification(
				"This is a test of the sms notification System", "");
	}

	@Test
	public void testSendHtmlNotificationsForAccount() {
		Long testAccountId = 1L;
		notificationService.sendAccountHTMLNotifications(testAccountId);
	}

	@Test
	public void testSendSMSNotificationForAccount() {
		Long testAccountId = 1L;
		notificationService.sendAccountSMSNotifications(testAccountId);
	}

	@Test
	public void testSendAllNotifications() {
		notificationService.sendNotifications();
	}
}
