package com.custanalytics.enotification.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.custanalytics.enotification.rest.entity.Notification;
import com.custanalytics.enotification.service.EmailNotificationService;

public class EmailServiceTest extends BaseTest {

	@Autowired
	EmailNotificationService emailService;

	@Test
	public void testEmailNotificationServiceTest() {

		emailService.emailServiceStatusCheck();
	}

	@Test
	public void testSendEmailNotification() {

		Notification mailNotification = createMailNotification();
		boolean mailSent = emailService.sendMailNotification(mailNotification);

		Assert.assertTrue(mailSent, " Mail was not sent");

	}

	@Test
	public void testSendHttpClientEmailNotification() {
		Notification mailNotification = createMailNotification();
		boolean sent =emailService.sendMailNotification(mailNotification);

		Assert.assertTrue(sent, "Was not able to send email notification with the Http Client");

	}

	private Notification createMailNotification() {
		Notification mailNotification = new Notification();
		/**
		 * 		map.add("from", "dmuruli@custanalytics.com");
		map.add("to", "dmuruli@gmail.com");
		map.add("subject", "Test Email Notification");
		map.add("text", "Unit Test Email");
		 */

		mailNotification.setFrom("dmuruli@custanalytics.com");
		mailNotification
				.setHtmlMessage("<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>Hello World Test Email</title>"
						+ "</head><body><br>Hello World</br></body></html>");
		mailNotification.setPlainText("Unit Test Email");
		mailNotification.setSubject("Mail Notification unit test");
		mailNotification.setTo("dmuruli@gmail.com");// parameterize and read
													// from a property file
		return mailNotification;
	}

}
